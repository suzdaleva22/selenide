import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

public class QaAutoTest {

    @Test
    public void checkLogo() {
        open("https://guest:welcome2qauto@qauto.forstudy.space/panel/garage");
        $("body > app-root > app-global-layout > div > div > app-header > header > div > div > div > a > svg").shouldBe(visible);

    }

    @Test
    public void loginAsGuest() {
        open("https://guest:welcome2qauto@qauto.forstudy.space/panel/garage");
        $("body > app-root > app-global-layout > div > div > app-header > header > div > div > div.header_right.d-flex.align-items-center > button.header-link.-guest").click();
        $("body > app-root > app-global-layout > div > div > div > app-panel-layout > div > div > div > div.col-lg-9.main-wrapper > div > app-garage > div > div.panel-page_heading.d-flex.justify-content-between > h1").shouldHave(exactText("Garage"));
    }

    @Test
    public void checkTabs() {
        loginAsGuest();
        $("body > app-root > app-global-layout > div > div > div > app-panel-layout > div > div > div > div.col-3.d-none.d-lg-block.sidebar-wrapper > nav > a:nth-child(2)").click();
        $("body > app-root > app-global-layout > div > div > div > app-panel-layout > div > div > div > div.col-lg-9.main-wrapper > div > app-fuel-expenses > div > div.panel-page_heading.d-flex.flex-column.flex-lg-row > h1").shouldHave(text("Fuel expenses"));
        $("body > app-root > app-global-layout > div > div > div > app-panel-layout > div > div > div > div.col-3.d-none.d-lg-block.sidebar-wrapper > nav > a:nth-child(3)").click();
      //  $("body > app-root > app-global-layout > div > div > div > app-panel-layout > div > div > div > div.col-lg-9.main-wrapper > div > app-instructions > div > div.panel-page_heading.d-flex.justify-content-between > h1").shouldHave(text("Instructions"));
        $("body > app-root > app-global-layout > div > div > div > app-panel-layout > div > div > div > div.col-lg-9.main-wrapper > div > app-instructions > div > div.panel-page_heading.d-flex.justify-content-between > h1").shouldHave(text("Some text"));

    }

  @Test
    public void checkProfileMenu() {
      loginAsGuest();
      $("#userNavDropdown").click();
      $("body > app-root > app-global-layout > div > div > app-header > header > div > div > app-user-nav > div > nav").shouldBe(visible);

  }

  @Test
    public void addNewCar() {
      loginAsGuest();
      $("body > app-root > app-global-layout > div > div > div > app-panel-layout > div > div > div > div.col-lg-9.main-wrapper > div > app-garage > div > div.panel-page_heading.d-flex.justify-content-between > button").click();
      $("#addCarMileage").setValue("50");
      $("body > ngb-modal-window > div > div > app-add-car-modal > div.modal-footer.d-flex.justify-content-end > button.btn.btn-primary").click();
      $("body > app-root > app-global-layout > div > div > div > app-panel-layout > div > div > div > div.col-lg-9.main-wrapper > div > app-garage > div > div.panel-page_content > div > ul > li:nth-child(1) > app-car > div").shouldBe(visible);

  }

  @Test
    public void logout() {
      loginAsGuest();
      $("body > app-root > app-global-layout > div > div > div > app-panel-layout > div > div > div > div.col-3.d-none.d-lg-block.sidebar-wrapper > nav > a.btn.btn-link.text-danger.btn-sidebar.sidebar_btn").click();
      $("body > app-root > app-global-layout > div > div > app-header > header > div > div > div.header_right.d-flex.align-items-center > button.header-link.-guest").shouldBe(visible);
  }

}
